#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 1024
# endif

# include <stdlib.h>
# include <unistd.h>

int						get_next_line(char **line);
int						ft_strlen(char *str);
void					*ft_memcpy(void *dst, const void *src, size_t n);
char					*ft_strjoin(char *s1, char *s2);
char					*ft_strdup(char *str);
void					ft_free(char **ptr);
char					*ft_strchr(const char *s, int c);

#endif